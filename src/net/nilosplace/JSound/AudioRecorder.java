package net.nilosplace.JSound;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.TargetDataLine;

public class AudioRecorder extends Thread {
	private TargetDataLine tdl;
	private AudioFileFormat.Type afft;
	private AudioInputStream ais;
	private File outfile;

	public AudioRecorder(TargetDataLine line) {
		tdl = line;
		ais = new AudioInputStream(line);
		afft = AudioFileFormat.Type.WAVE;
		outfile = new File("test.wav");
	}

	public void startRecording() {
		tdl.start();
		super.start();
	}

	public void stopRecording() {
		tdl.stop();
		tdl.close();
	}

	public void run() {
		try {
			AudioSystem.write(ais, afft, outfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
