package net.nilosplace.JSound;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Line;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.Mixer.Info;

public class Main {

	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		Mixer.Info[] infos = AudioSystem.getMixerInfo();
		for(Info i: infos) {
			if(i.getName().equals("Built-in Input")) {
				System.out.println(i.getName() + ": ");
				 try {
					Mixer mixer = AudioSystem.getMixer(i);
					Line.Info[] li = mixer.getSourceLineInfo();
					//System.out.println(li);
					for(Line.Info i2: li) {
						System.out.println(i2.getLineClass());
						Line sid = mixer.getLine(i2);
						System.out.println(sid);
						//sid.open();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
