package net.nilosplace.JSound;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Line;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.Mixer.Info;
import javax.sound.sampled.TargetDataLine;

public class SimpleAudioRecorder extends Thread {


	public static void main(String[] args) throws Exception {

		AudioFormat	audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100.0F, 16, 2, 4, 44100.0F, false);
		//AudioFormat	audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100.0f, 16, 1, 2, 44100.0f, true);
		
		
		Mixer.Info[] infos = AudioSystem.getMixerInfo();
		TargetDataLine targetDataLine = null;
		for(Info i: infos) {
			if(i.getName().equals("Built-in Input")) {
				Mixer mixer = AudioSystem.getMixer(i);
				Line.Info[] info = mixer.getTargetLineInfo();
				for(Line.Info i2: info) {
					targetDataLine = (TargetDataLine) AudioSystem.getLine(i2);
					targetDataLine.open(audioFormat);
					break;
				}
				break;
			}
		}

		AudioRecorder recorder = new AudioRecorder(targetDataLine);
		System.out.println("Recording for 20 seconds");
		recorder.startRecording();
		sleep(20000);
		recorder.stopRecording();
		System.out.println("Recording stopped.");
	}
}
