package org.jsresources.apps.chat;

import static org.jsresources.apps.chat.Constants.AUDIO_PROPERTY;
import static org.jsresources.apps.chat.Constants.BUFFER_SIZE_MILLIS_STR;
import static org.jsresources.apps.chat.Constants.DEBUG;
import static org.jsresources.apps.chat.Constants.DIR_MIC;
import static org.jsresources.apps.chat.Constants.DIR_SPK;
import static org.jsresources.apps.chat.Constants.out;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.sound.sampled.FloatControl;
import javax.sound.sampled.Port;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class AudioPanel extends JPanel implements ActionListener, ItemListener, ChangeListener, PropertyChangeListener {

	private static final long serialVersionUID = -8589938174361663228L;
	private JButton disconnectButton;
	private JButton testButton;

	private JComboBox[] mixerSelector = new JComboBox[2];
	private JProgressBar[] volumeMeter = new JProgressBar[2];
	private JSlider[] volumeSlider = new JSlider[2];
	private JComboBox[] volumePort = new JComboBox[2];
	private JCheckBox[] muteButton = new JCheckBox[2];

	private JComboBox[] bufferSelector = new JComboBox[2];
	private ChatModel chatmodel;
	private AudioSettings settings;
	
	public AudioPanel(ChatModel m_chatModel, AudioSettings m_audioSettings) {
		chatmodel = m_chatModel;
		settings = m_audioSettings;
		setLayout(new GridLayout(1,2));
	
		createGUI(0, "Microphone");
		createGUI(1, "Speaker");
	
		chatmodel.addPropertyChangeListener(this);
		enableButtons();
	}

	private void createGUI(int d, String title) {
		JPanel p = new JPanel();
		p.setLayout(new StripeLayout(0, 2, 0, 2, 3));
		p.add(new JLabel(title));
	
		p.add(new JLabel("Mixer:"));
		mixerSelector[d] = new JComboBox(settings.getMixers(d).toArray());
		mixerSelector[d].addItemListener(this);
		p.add(mixerSelector[d]);
	
		p.add(new JLabel("Level:"));
		volumeMeter[d] = new JProgressBar(JProgressBar.HORIZONTAL, 0, 128);
		p.add(volumeMeter[d]);
	
		volumeSlider[d] = new JSlider(0, 100, 100);
		volumeSlider[d].addChangeListener(this);
		p.add(volumeSlider[d]);
	
		volumePort[d] = new JComboBox(settings.getPorts(d).toArray());
		volumePort[d].addItemListener(this);
		p.add(volumePort[d]);
	
		muteButton[d] = new JCheckBox("Mute");
		muteButton[d].addItemListener(this);
		p.add(muteButton[d]);
	
		p.add(new JLabel("Buffer size in millis:"));
		bufferSelector[d] = new JComboBox(BUFFER_SIZE_MILLIS_STR);
		bufferSelector[d].setSelectedIndex(settings.getBufferSizeIndex(d));
		bufferSelector[d].addItemListener(this);
		p.add(bufferSelector[d]);
	
		if (d == DIR_SPK) {
			disconnectButton = new JButton("Disconnect");
			disconnectButton.addActionListener(this);
			p.add(disconnectButton);
		} else {
			testButton = new JButton("Start Mic Test");
			testButton.addActionListener(this);
			p.add(testButton);
		}
		add(p);
		// init with first port
		settings.setSelPort(d, 0);
		initNewPort(d);
	}

	private AudioBase getAudio(int d) {
		return chatmodel.getAudio(d);
	}

	private void initNewPort(int d) {
		Port port = settings.getSelPort(d);
		FloatControl c = settings.getSelVolControl(d);
		volumeSlider[d].setEnabled(port != null && c != null);
		updateVolumeSlider(d);
	}

	private void updateVolumeSlider(int d) {
		FloatControl c = settings.getSelVolControl(d);
		if (c != null && volumeSlider[d].isEnabled()) {
			int newPos = (int) (((c.getValue() - c.getMinimum()) / (c.getMaximum() - c.getMinimum())) * 100.0f);
			if (newPos != volumeSlider[d].getValue()) {
			volumeSlider[d].setValue(newPos);
			//if (VERBOSE) out("Setting slider to: "+newPos);
			}
		}
	}

	private void updateVolume(int d) {
		FloatControl c = settings.getSelVolControl(d);
		if (c != null && volumeSlider[d].isEnabled()) {
	
			float newVol = ((volumeSlider[d].getValue() / 100.0f) * (c.getMaximum() - c.getMinimum())) + c.getMinimum();
			c.setValue(newVol);
			//if (VERBOSE) out("Setting vol: "+newVol);
		}
	}

	private void initNewMixer(int d) {
		try {
			getAudio(d).setMixer(settings.getSelMixer(d));
			if (d == DIR_MIC) {
				chatmodel.initAudioStream();
			}
		} catch (Exception e) {
			if (DEBUG) e.printStackTrace();
			out(e.getMessage());
		}
	}

	private void initNewBufferSize(int d) {
		try {
			getAudio(d).setBufferSizeMillis(settings.getBufferSizeMillis(d));
			if (d == DIR_MIC) {
				chatmodel.initAudioStream();
			}
		} catch (Exception e) {
			if (DEBUG) e.printStackTrace();
			out(e.getMessage());
		}
	}

	public void itemStateChanged(ItemEvent e) {
		int d = -1;
		if (e.getSource() == volumePort[0]) {
			d = 0;
		} else if (e.getSource() == volumePort[1]) {
			d = 1;
		}
		if ((d >= 0) && (e.getStateChange() == ItemEvent.SELECTED)) {
			settings.setSelPort(d, volumePort[d].getSelectedIndex());
			initNewPort(d);
			return;
		}
		d = -1;
		if (e.getSource() == mixerSelector[0]) {
			d = 0;
		} else if (e.getSource() == mixerSelector[1]) {
			d = 1;
		}
		if ((d >= 0) && (e.getStateChange() == ItemEvent.SELECTED)) {
			settings.setSelMixer(d, mixerSelector[d].getSelectedIndex());
			initNewMixer(d);
			return;
		}
		d = -1;
		if (e.getSource() == bufferSelector[0]) {
			d = 0;
		} else if (e.getSource() == bufferSelector[1]) {
			d = 1;
		}
		if ((d >= 0) && (e.getStateChange() == ItemEvent.SELECTED)) {
			settings.setBufferSizeIndex(d, bufferSelector[d].getSelectedIndex());
			initNewBufferSize(d);
			return;
		}
	
		d = -1;
		if (e.getSource() == muteButton[0]) {
			d = 0;
		} else if (e.getSource() == muteButton[1]) {
			d = 1;
		}
		if (d >= 0 && getAudio(d) != null) {
			getAudio(d).setMuted(muteButton[d].isSelected());
		}
	}

	public void stateChanged(ChangeEvent e) {
		int d;
		if (e.getSource() == volumeSlider[0]) {
			d = 0;
		} else if (e.getSource() == volumeSlider[1]) {
			d = 1;
		} else {
			return;
		}
		updateVolume(d);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == testButton) {
			chatmodel.toggleTestAudio();
		} else 	if (e.getSource() == disconnectButton) {
			chatmodel.disconnect();
		}
	}

	private void enableButtons() {
		if (chatmodel.isConnected()) {
			disconnectButton.setEnabled(true);
			testButton.setEnabled(false);
			testButton.setText("Start Mic Test");
		} else {
			disconnectButton.setEnabled(false);
			testButton.setEnabled(true);
			if (chatmodel.isAudioActive()) {
			testButton.setText("Stop Mic Test");
			} else {
			testButton.setText("Start Mic Test");
			}
		}
	}

	public void propertyChange(PropertyChangeEvent e) {
		if (DEBUG) out("Property change. " + "AudioActive:" + chatmodel.isAudioActive() + "Connected:" + chatmodel.isConnected());
		if (e.getPropertyName().equals(AUDIO_PROPERTY)) {
			if (chatmodel.isAudioActive()) {
			startLevelMeterThread();
			}
		}
		enableButtons();
	}

	private void startLevelMeterThread() {
		new Thread(new Runnable() {
			public void run() {
				if (DEBUG) out("Meter Thread: start");
				try {
					while (chatmodel.isAudioActive()) {
						for (int d = 0; d < 2; d++) {
							AudioBase ab = getAudio(d);
							if (ab != null) {
								int level = ab.getLevel();
								if (level >= 0) {
									volumeMeter[d].setValue(level);
								} else {
									volumeMeter[d].setValue(0);
								}
							}
						}
						Thread.sleep(30);
					}
	
				} catch (Exception e) {
					e.printStackTrace();
				}
				volumeMeter[0].setValue(0);
				volumeMeter[1].setValue(0);
				if (DEBUG) out("Meter Thread: stop");
			}
		}).start();
	}
}
