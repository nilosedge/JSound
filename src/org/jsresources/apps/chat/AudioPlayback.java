package org.jsresources.apps.chat;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;

import static org.jsresources.apps.chat.Constants.DEBUG;
import static org.jsresources.apps.chat.Constants.VERBOSE;
import static org.jsresources.apps.chat.Constants.out;


// Class that reads its audio from an AudioInputStream
public class AudioPlayback extends AudioBase {

	private static final boolean DEBUG_TRANSPORT = false;

	protected AudioInputStream ais;
	private PlayThread thread;

	public AudioPlayback(int formatCode, Mixer mixer, int bufferSizeMillis) {
		super("Speaker", formatCode, mixer, bufferSizeMillis);
	}

	protected void createLineImpl() throws Exception {
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, lineFormat);
	
		// get the playback data line for capture.
		if (mixer != null) {
			line = (SourceDataLine) mixer.getLine(info);
		} else {
			line = AudioSystem.getSourceDataLine(lineFormat);
		}
	}

	protected void openLineImpl() throws Exception {
		SourceDataLine sdl = (SourceDataLine) line;
		System.out.println(lineFormat);
		sdl.open(lineFormat, bufferSize);
	}

	public synchronized void start() throws Exception {
		boolean needStartThread = false;
		if (thread != null && thread.isTerminating()) {
			thread.terminate();
			needStartThread = true;
		}
		if (thread == null || needStartThread) {
			// start thread
			thread = new PlayThread();
			thread.start();
		}
		super.start();
	}

	protected void closeLine(boolean willReopen) {
		PlayThread oldThread = null;
		synchronized(this) {
			if (!willReopen && thread != null) {
				thread.terminate();
			}
			super.closeLine(willReopen);
			if (!willReopen && thread != null) {
				oldThread = thread;
				thread = null;
			}
		}
		if (oldThread != null) {
			if (VERBOSE) out("AudioPlayback.closeLine(): closing thread, waiting for it to die");
			oldThread.waitFor();
			if (VERBOSE) out("AudioPlayback.closeLine(): thread closed");
		}
	}

	// in network format
	public void setAudioInputStream(AudioInputStream ais) {
		this.ais = AudioSystem.getAudioInputStream(lineFormat, ais);
	}

	class PlayThread extends Thread {
		private boolean doTerminate = false;
		private boolean terminated = false;
		// for debugging
		private boolean printedBytes = false;
	
		public void run() {
			if (VERBOSE) out("Start AudioPlayback pull thread");
			byte[] buffer = new byte[getBufferSize()];
			try {
				while (!doTerminate) {
					SourceDataLine sdl = (SourceDataLine) line;
					if (ais != null) {
						int r = ais.read(buffer, 0, buffer.length);
						if (r > 50 && DEBUG_TRANSPORT && !printedBytes) {
							printedBytes = true;
							out("AudioPlayback: first bytes being played:");
							String s = "";
							for (int i = 0; i < 50; i++) {
							s+=" "+buffer[i];
							}
							out(s);
						}
						if (r > 0) {
							if (isMuted()) {
								muteBuffer(buffer, 0, r);
							}
							// run some simple analysis
							calcCurrVol(buffer, 0, r);
							if (sdl != null) {
								sdl.write(buffer, 0, r);
							}
						} else {
							if (r == 0) {
								synchronized(this) {
									this.wait(40);
								}
							}
						}
					} else {
						synchronized(this) {
							this.wait(50);
						}
					}
				}
			} catch (IOException ioe) {
				//if (DEBUG) ioe.printStackTrace();
			} catch (InterruptedException ie) {
				if (DEBUG) ie.printStackTrace();
			}
			if (VERBOSE) out("Stop AudioPlayback pull thread");
			terminated = true;
		}
	
		public synchronized void terminate() {
			doTerminate = true;
			this.notifyAll();
		}
	
		public synchronized boolean isTerminating() {
			return doTerminate || terminated;
		}
	
		public synchronized void waitFor() {
			if (!terminated) {
				try {
					this.join();
				} catch (InterruptedException ie) {
					if (DEBUG) ie.printStackTrace();
				}
			}
		}
	}
}
