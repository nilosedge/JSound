package org.jsresources.apps.chat;

import static org.jsresources.apps.chat.Constants.AUDIO_PROPERTY;
import static org.jsresources.apps.chat.Constants.CONNECTION_PROPERTY;
import static org.jsresources.apps.chat.Constants.CONNECTION_TYPE_TCP;
import static org.jsresources.apps.chat.Constants.DIR_MIC;
import static org.jsresources.apps.chat.Constants.DIR_SPK;
import static org.jsresources.apps.chat.Constants.FORMAT_CODES;
import static org.jsresources.apps.chat.Constants.PROTOCOL_ACK;
import static org.jsresources.apps.chat.Constants.PROTOCOL_ERROR;
import static org.jsresources.apps.chat.Constants.PROTOCOL_MAGIC;
import static org.jsresources.apps.chat.Constants.PROTOCOL_VERSION;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;


public class ChatModel {

	private PropertyChangeSupport m_propertyChangeSupport;
	private Network			m_network;
	private ListenThread		m_listenThread;
	private DataInputStream		m_receiveStream;
	private OutputStream		m_sendStream;

	// audio related: owned by ChatModel
	private AudioBase[] audio = new AudioBase[2];
	private boolean m_audioActive;
	private ConnectionSettings csettings;
	private AudioSettings asettings;

	public ChatModel(ConnectionSettings settings, AudioSettings m_audioSettings) {
		this.csettings = settings;
		this.asettings = m_audioSettings;
		m_propertyChangeSupport = new PropertyChangeSupport(this);
		initNetwork();
		audio[DIR_MIC] = new AudioCapture(settings.getFormatCode(), asettings.getSelMixer(DIR_MIC), asettings.getBufferSizeMillis(DIR_MIC));
		audio[DIR_SPK] = new AudioPlayback(settings.getFormatCode(), asettings.getSelMixer(DIR_SPK), asettings.getBufferSizeMillis(DIR_SPK));
		m_audioActive = false;
	}

	private void initNetwork() {
		if (csettings.getConnectionType() == CONNECTION_TYPE_TCP) {
			m_network = new TCPNetwork(csettings);
		} else {
			m_network = new UDPNetwork(csettings);
		}
	}

	public AudioBase getAudio(int d) {
		return audio[d];
	}

	/** Connect to a given host or IP.
	@param strHostname a hostname or an IP address in the form
	xx.xx.xx.xx
	*/
	public void connect(String strHostname) {
		InetAddress addr = null;
		try {
			addr = InetAddress.getByName(strHostname);
		} catch (UnknownHostException e) {
			Debug.out(e);
			JOptionPane.showMessageDialog(null, "Unknown host " + strHostname);
		}
		if (addr != null) {
			m_network.connect(addr);
		}
		if (! m_network.isConnected()) {
			JOptionPane.showMessageDialog(null, new Object[]{"Could not establish connection to " + strHostname}, "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			initConnection(true);
		}
	}

	public void disconnect() {
		Debug.out("closing audio...");
		closeAudio();
		Debug.out("...closed");
		if (m_network.isConnected()) {
			Debug.out("diconnecting network...");
			m_network.disconnect();
			Debug.out("disconnected...");
			notifyConnection();
		}
	}

	public void setListen(boolean bListen) {
		if (bListen != (m_listenThread != null)) {
			if (bListen) {
				m_listenThread = new ListenThread();
				m_listenThread.start();
			} else {
				m_listenThread.setTerminate();
			}
		}
	}

	/** Set up after socket connection has been established.
	This negotiates the audio format and inits the streams.

	@param bActive true if called for the initiating (active)
	endpoint. false for the accepting (passive) endpoint.
	*/
	private void initConnection(boolean bActive) {
		Debug.out("initConnection(" + bActive + "): begin");
		try {
			m_receiveStream = new DataInputStream(m_network.createReceiveStream());
			m_sendStream = m_network.createSendStream();
			Debug.out("initConnection(): receiveStream: " + m_receiveStream);
			Debug.out("initConnection(): sendStream: " + m_sendStream);
		} catch (IOException e) {
			Debug.out(e);
			streamError("Problems while setting up the connection");
		}
	
		/* To agree on the audio data format, the active side sends a
		   32 bit integer format code so that the passive side can
		   adapt to it.
	
		   This mechanism could be extended to a real negotiation
		   where the passive side sends a list of possible formats and
		   the active one chooses one of them. */
		boolean bHandshakeSuccessful = false;
		if (bActive) {
			bHandshakeSuccessful = doHandshakeActive();
		} else {
			bHandshakeSuccessful = doHandshakePassive();
		}
		if (bHandshakeSuccessful) {
			Debug.out("connection established");
			if (m_network.isConnected()) {
				initNetworkAudio();
			}
			notifyConnection();
		} else {
			m_receiveStream = null;
			m_sendStream = null;
		}
		Debug.out("initConnection(" + bActive + "): end");
	}
	
	
	// returns true if successful
	private boolean doHandshakeActive() {
		DataOutputStream dos = new DataOutputStream(m_sendStream);
		// phase I
		try {
			dos.writeInt(PROTOCOL_MAGIC);
			Debug.out("written magic: " + PROTOCOL_MAGIC);
			dos.writeInt(PROTOCOL_VERSION);
			Debug.out("written protocol version: " + PROTOCOL_VERSION);
			dos.writeInt(csettings.getFormatCode());
			Debug.out("written format code: " + csettings.getFormatCode());
		} catch (IOException e) {
			Debug.out(e);
			streamError("I/O error during handshake (active, phase I)");
			return false;
		}
	
		// phase II
		byte[] abBuffer = new byte[4];
		try {
			m_receiveStream.readFully(abBuffer);
		} catch (IOException e) {
			Debug.out(e);
			streamError("I/O error during handshake (active, phase II)");
			return false;
		}
		//Debug.out("abBuffer[0]: " + abBuffer[0]);
		//Debug.out("abBuffer[1]: " + abBuffer[1]);
		//Debug.out("abBuffer[2]: " + abBuffer[2]);
		//Debug.out("abBuffer[3]: " + abBuffer[3]);
		int w = ((abBuffer[0] &0xFF) << 24) | ((abBuffer[1] & 0xFF) << 16) | ((abBuffer[2] & 0xFF) << 8) | (abBuffer[3] & 0xFF);
		//Debug.out("received code: " + w);
		if (w != PROTOCOL_ACK) {
			streamError("error on remote peer");
			return false;
		}
		return true;
	}


	// returns true if successful
	private boolean doHandshakePassive() {
		boolean bSuccess = true;
	
		// phase I
		byte[] abBuffer = new byte[12];
		try {
			int nRead = m_receiveStream.read(abBuffer);
			if (nRead != 12) {
				streamError("I/O Error during handshake (passive, phase I)");
				bSuccess = false;
			}
		} catch (IOException e) {
			Debug.out(e);
			streamError("I/O error during handshake (passive, phase I)");
			bSuccess = false;
		}
		if (bSuccess) {
			int w = (abBuffer[0] << 24) | (abBuffer[1] << 16) | (abBuffer[2] << 8) | abBuffer[3];
			Debug.out("received magic: " + w);
			if (w != PROTOCOL_MAGIC) {
				streamError("wrong magic");
				bSuccess = false;
			} else {
				w = (abBuffer[4] << 24) | (abBuffer[5] << 16) | (abBuffer[6] << 8) | abBuffer[7];
				Debug.out("received protocol version: " + w);
				if (w != PROTOCOL_VERSION) {
					streamError("wrong protocol version");
					bSuccess = false;
				} else {
					w = (abBuffer[8] << 24) | (abBuffer[9] << 16) | (abBuffer[10] << 8) | abBuffer[11];
					Debug.out("received format code: " + w);
					if (w < 0 || w > FORMAT_CODES.length) {
						streamError("wrong format code");
						bSuccess = false;
					}
				}
			}
		}
	
		// phase II
		DataOutputStream dos = new DataOutputStream(m_sendStream);
		try {
			if (bSuccess) {
				dos.writeInt(PROTOCOL_ACK);
				Debug.out("written ACK");
			} else {
				dos.writeInt(PROTOCOL_ERROR);
				Debug.out("written ERROR");
			}
		} catch (IOException e) {
			Debug.out(e);
			streamError("I/O error during handshake (passive, phase II)");
			bSuccess = false;
		}
		return bSuccess;
	}


	public boolean isConnected() {
		return m_network.isConnected();
	}


	/* Set up audio connections. */
	private void initNetworkAudio() {
		//Debug.out("initNetworkAudio(): receiveStream: " + getReceiveStream());
		//Debug.out("initNetworkAudio(): sendStream: " + getSendStream());
		try {
			Debug.out("audio: " + audio[DIR_MIC]);
			((AudioCapture) audio[DIR_MIC]).setOutputStream(m_sendStream);
			((AudioPlayback) audio[DIR_SPK]).setAudioInputStream(AudioUtils.createNetAudioInputStream(csettings.getFormatCode(), m_receiveStream));
			startAudio(DIR_MIC);
			startAudio(DIR_SPK);
			setAudioActive(true);
		} catch (Exception e) {
			Debug.out(e);
			streamError(e.getMessage());
		}
	}

	public void initAudioStream() {
		// only necessary for test mode on microphone side
		if (isMicrophoneTest()) {
			((AudioPlayback) audio[DIR_SPK]).setAudioInputStream(((AudioCapture) audio[DIR_MIC]).getAudioInputStream());
		}
	}

	private void closeAudio() {
		setAudioActive(false);
		closeAudio(DIR_SPK);
		closeAudio(DIR_MIC);
	}

	public boolean isMicrophoneTest() {
		return m_audioActive && (((AudioCapture) audio[DIR_MIC]).getOutputStream() == null);
	}

	public boolean isAudioActive() {
		return m_audioActive;
	}

	public void setAudioActive(boolean active) {
		m_audioActive = active;
		notifyAudio();
	}

	private void closeAudio(int d) {
		if (audio[d] != null) {
			audio[d].close();
		}
	}

	private void startAudio(int d) throws Exception {
		if (!m_audioActive) {
			audio[d].setFormatCode(csettings.getFormatCode());
		}
		audio[d].open();
		audio[d].start();
	}


	// returns if testing audio
	public void toggleTestAudio() {
		if (m_network.isConnected()) {
			Debug.out("Call to ChatModel.toggleTestAudio, but connection active!");
			return;
		}
		try {
			if (m_audioActive) {
				closeAudio(DIR_MIC);
				closeAudio(DIR_SPK);
				setAudioActive(false);
			} else {
				startAudio(DIR_MIC);
				((AudioCapture) audio[DIR_MIC]).setOutputStream(null);
				startAudio(DIR_SPK);
				setAudioActive(true);
				initAudioStream();
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, new Object[]{"Error: ", ex.getMessage()}, "Error", JOptionPane.ERROR_MESSAGE);
			closeAudio(0);
			closeAudio(1);
			setAudioActive(false);
			notifyAudio();
		}
	}


	private void streamError(String strError) {
		JOptionPane.showMessageDialog(null, new Object[]{strError, "Connection will be terminated"}, "Error", JOptionPane.ERROR_MESSAGE);
		m_network.disconnect();
		closeAudio();
		notifyConnection();
	}


	/*
	 * Properties Support
	 */

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		m_propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		m_propertyChangeSupport.removePropertyChangeListener(listener);
	}


	private void notifyConnection() {
		m_propertyChangeSupport.firePropertyChange(CONNECTION_PROPERTY, m_network.isConnected(), ! m_network.isConnected());
	}


	private void notifyAudio() {
		m_propertyChangeSupport.firePropertyChange(AUDIO_PROPERTY, m_audioActive, ! m_audioActive);
	}

	/*
	 * Inner Classes
	 */
	private class ListenThread extends Thread {
		private boolean m_bTerminate;

		public void setTerminate() {
			m_bTerminate = true;
		}

		public void run() {
			/* For the termination mechanism to work, it is necessary
			 * that listen() has a finite timeout (it must not block
			 * forever) or that setListen(false) interrupts it. */
			m_network.setListen(true);
			while (!m_bTerminate) {
				if (m_network.listen()) {
					String strMessage = "Call received from " + m_network.getPeer() + ". Do you want to accept?";
					int nAnswer = JOptionPane.showConfirmDialog(null, new Object[]{strMessage}, "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if (nAnswer == JOptionPane.YES_OPTION) {
						initConnection(false);
						setListen(false);
					} else {
						m_network.disconnect();
					}
				}
			}
			m_network.setListen(false);
		}
	}
}
