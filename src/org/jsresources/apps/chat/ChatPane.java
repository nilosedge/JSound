package org.jsresources.apps.chat;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;



public class ChatPane extends JPanel {
	private static final long serialVersionUID = 1804894127487070629L;
	private JLabel m_statusLabel;
	
	private ChatModel m_chatModel;
	private ConnectionSettings m_connectionSettings;
	private AudioSettings m_audioSettings;

	public ChatPane() {
		super();

		m_connectionSettings = new ConnectionSettings();
		m_audioSettings = new AudioSettings();
		m_chatModel = new ChatModel(m_connectionSettings, m_audioSettings);
		
		
		setLayout(new BorderLayout());
		JComponent tabbed = createTabbedPane();
		this.add(tabbed, BorderLayout.CENTER);
		m_statusLabel = new JLabel();
		this.add(m_statusLabel, BorderLayout.SOUTH);

		m_statusLabel.setText(" ");
	}


	private JComponent createTabbedPane() {
		JTabbedPane tabbedPane = new JTabbedPane();
		JComponent	component;
		component = new ConnectionPanel(m_chatModel);
		tabbedPane.add(component, "Connection");

		component = new AudioPanel(m_chatModel, m_audioSettings);
		tabbedPane.add(component, "Audio");

		component = new SettingsPanel(m_connectionSettings);
		tabbedPane.add(component, "Settings");

		return tabbedPane;
	}

}
