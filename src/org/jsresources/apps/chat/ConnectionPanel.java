package org.jsresources.apps.chat;

import static org.jsresources.apps.chat.Constants.CONNECTION_PROPERTY;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ConnectionPanel extends JPanel implements ActionListener, ItemListener, PropertyChangeListener {
	private static final long serialVersionUID = 5441643489966195346L;

	private JTextField		m_nameTextField;
	private JButton			m_connectButton;
	private ChatModel		chatmodel;

	public ConnectionPanel(ChatModel m_chatModel) {
		chatmodel = m_chatModel;
		setLayout(new GridLayout(0, 1));

		JPanel panel = null;

		panel = new JPanel();
		panel.add(new JLabel("Name or IP:"));
		m_nameTextField = new JTextField(20);
		panel.add(m_nameTextField);
		add(panel);

		panel = new JPanel();
		m_connectButton = new JButton("Connect...");
		m_connectButton.addActionListener(this);
		m_connectButton.setActionCommand("connect");
		panel.add(m_connectButton);
		add(panel);


		panel = new JPanel();
		JCheckBox listenCheckBox = new JCheckBox("Listen to incoming connections", true);
		listenCheckBox.addItemListener(this);
		panel.add(listenCheckBox);
		add(panel);
		chatmodel.addPropertyChangeListener(this);
		chatmodel.setListen(true);
	}

	public void itemStateChanged(ItemEvent e) {
		switch (e.getStateChange()) {
			case ItemEvent.DESELECTED:
				chatmodel.setListen(false);
				break;
			case ItemEvent.SELECTED:
				chatmodel.setListen(true);
				break;
		}
	}


	public void actionPerformed(ActionEvent ae) {
		String	strActionCommand = ae.getActionCommand();
		if (strActionCommand.equals("connect")) {
			String strHostname = m_nameTextField.getText();
			chatmodel.connect(strHostname);
		}
	}


	/** Reflect the active state of the connection in the GUI.
		This method should be called each time the state of the
		connection changes (i.e. disconnected -> connected,
		connected -> disconnected).
	 */
	private void setConnectionActive(boolean bActive) {
		m_connectButton.setEnabled(! bActive);
	}


	public void propertyChange(PropertyChangeEvent e) {
		if (e.getPropertyName().equals(CONNECTION_PROPERTY)) {
			boolean bConnected = ((Boolean) e.getNewValue()).booleanValue();
			setConnectionActive(bConnected);
		}
	}
}
