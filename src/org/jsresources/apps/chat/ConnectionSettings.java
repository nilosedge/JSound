
package org.jsresources.apps.chat;

import static org.jsresources.apps.chat.Constants.DEFAULT_CONNECTION_TYPE;
import static org.jsresources.apps.chat.Constants.DEFAULT_FORMAT_CODE;
import static org.jsresources.apps.chat.Constants.DEFAULT_PORT;

public class ConnectionSettings {
	private int		m_nPort;
	private int		m_nConnectionType;
	private int		m_nFormatCode;

	public ConnectionSettings() {
		setPort(DEFAULT_PORT);
		setFormatCode(DEFAULT_FORMAT_CODE);
		setConnectionType(DEFAULT_CONNECTION_TYPE);
	}

	public void setPort(int nPort) {
		m_nPort = nPort;
	}

	public int getPort() {
		return m_nPort;
	}

	public void setConnectionType(int nConnectionType) {
		m_nConnectionType = nConnectionType;
	}

	public int getConnectionType() {
		return m_nConnectionType;
	}

	public void setFormatCode(int nFormatCode) {
		m_nFormatCode = nFormatCode;
	}

	public int getFormatCode() {
		return m_nFormatCode;
	}
}
