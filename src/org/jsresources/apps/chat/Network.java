package org.jsresources.apps.chat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

public interface Network {
	public void connect(InetAddress addr);
	public void disconnect();
	public boolean isConnected();
	public InetAddress getPeer();
	public void setListen(boolean bListen);
	public boolean listen();
	public InputStream createReceiveStream() throws IOException;
	public OutputStream createSendStream() throws IOException;
}
