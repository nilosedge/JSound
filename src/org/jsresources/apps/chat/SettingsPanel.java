package org.jsresources.apps.chat;

import static org.jsresources.apps.chat.Constants.CONNECTION_TYPES;
import static org.jsresources.apps.chat.Constants.CONNECTION_TYPE_NAMES;
import static org.jsresources.apps.chat.Constants.FORMAT_CODES;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class SettingsPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 4058863685484462712L;
	private JTextField		m_portTextField;
	private JComboBox		m_typeComboBox;
	private JComboBox		m_qualityComboBox;
	private ConnectionSettings settings;

	public SettingsPanel(ConnectionSettings m_connectionSettings) {
		settings = m_connectionSettings;
		
		setLayout(new BorderLayout());

		JPanel	centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));

		JPanel p = new JPanel();
		p.add(new JLabel("Port:"));
		m_portTextField = new JTextField(5);
		p.add(m_portTextField);
		centerPanel.add(p);

		p = new JPanel();
		p.add(new JLabel("Connection Type:"));
		m_typeComboBox = new JComboBox(CONNECTION_TYPE_NAMES);
		p.add(m_typeComboBox);
		centerPanel.add(p);

		p = new JPanel();
		p.add(new JLabel("Audio Quality:"));
		m_qualityComboBox = new JComboBox(Constants.FORMAT_NAMES);
		p.add(m_qualityComboBox);
		centerPanel.add(p);

		add(centerPanel, BorderLayout.CENTER);

		JPanel	buttonPanel = new JPanel();

		JButton applyButton = new JButton("Apply");
		applyButton.setActionCommand("apply");
		applyButton.addActionListener(this);
		buttonPanel.add(applyButton);
		JButton resetButton = new JButton("Reset");
		resetButton.setActionCommand("reset");
		resetButton.addActionListener(this);
		buttonPanel.add(resetButton);

		add(buttonPanel, BorderLayout.SOUTH);

		reset();
	}

	public void actionPerformed(ActionEvent ae) {
		String	strActionCommand = ae.getActionCommand();
		if (strActionCommand.equals("apply")) {
			commit();
		} else if (strActionCommand.equals("reset")) {
			reset();
		}
	}


	private void commit() {
		int port = Integer.parseInt(m_portTextField.getText());
		settings.setPort(port);
		int nFormatCode = FORMAT_CODES[m_qualityComboBox.getSelectedIndex()];
		settings.setFormatCode(nFormatCode);
		int nConnectionType = CONNECTION_TYPES[m_typeComboBox.getSelectedIndex()];
		settings.setConnectionType(nConnectionType);
	}


	private void reset() {
		int port = settings.getPort();
		m_portTextField.setText("" + port);
		int nFormatCode = settings.getFormatCode();
		int nIndex = -1;
		for (int i = 0; i < FORMAT_CODES.length; i++) {
			if (nFormatCode == FORMAT_CODES[i]) {
				nIndex = i;
				break;
			}
		}
		m_qualityComboBox.setSelectedIndex(nIndex);

		int nConnectionType = settings.getConnectionType();
		nIndex = -1;
		for (int i = 0; i < CONNECTION_TYPES.length; i++) {
			if (nConnectionType == CONNECTION_TYPES[i]) {
				nIndex = i;
				break;
			}
		}
		m_typeComboBox.setSelectedIndex(nIndex);
	}
}
