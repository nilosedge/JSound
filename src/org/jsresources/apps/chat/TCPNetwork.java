package org.jsresources.apps.chat;

import static org.jsresources.apps.chat.Constants.TCP_NODELAY;
import static org.jsresources.apps.chat.Constants.TCP_RECEIVE_BUFFER_SIZE;
import static org.jsresources.apps.chat.Constants.TCP_SEND_BUFFER_SIZE;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;


public class TCPNetwork implements Network {
	private ServerSocket m_serverSocket;
	private Socket m_commSocket;
	private ConnectionSettings settings;

	public TCPNetwork(ConnectionSettings csettings) {
		settings = csettings;
	}

	public void connect(InetAddress addr) {
		Debug.out("connect(): begin");
		try {
			m_commSocket = new Socket(addr, settings.getPort());
			setSocketOptions(m_commSocket);
		} catch (IOException e) {
			Debug.out(e);
		}
	}

	public InetAddress getPeer() {
		return m_commSocket.getInetAddress();
	}

	public void disconnect() {
		try {
			m_commSocket.shutdownInput();
			m_commSocket.shutdownOutput();
		} catch (IOException e) {
			Debug.out(e);
		}
		try {
			m_commSocket.close();
		} catch (IOException e) {
			Debug.out(e);
		}
	}

	public boolean isConnected() {
		return m_commSocket != null && !m_commSocket.isClosed();
	}

	public void setListen(boolean bListen) {
		if (bListen != isListening()) {
			if (bListen) {
				try {
					m_serverSocket = new ServerSocket(settings.getPort());
					m_serverSocket.setSoTimeout(2000);
				} catch (IOException e) {
					Debug.out(e);
				}
			} else {
				try {
					m_serverSocket.close();
				} catch (IOException e) {
					Debug.out(e);
				}
				m_serverSocket = null;
			}
		}
	}

	private boolean isListening() {
		return m_serverSocket != null;
	}

	public boolean listen() {
		Socket s = null;
		try {
			s = m_serverSocket.accept();
			setSocketOptions(s);
		} catch (SocketTimeoutException e) {
			//Debug.out(e);
		} catch (IOException e) {
			Debug.out(e);
		}
		if (s != null) {
			m_commSocket = s;
			return true;
		} else {
			return false;
		}
	}

	public InputStream createReceiveStream() throws IOException {
		return m_commSocket.getInputStream();
	}

	public OutputStream createSendStream() throws IOException {
		return m_commSocket.getOutputStream();
	}

	private static void setSocketOptions(Socket socket) {
		try {
			socket.setTcpNoDelay(TCP_NODELAY);
			if (TCP_SEND_BUFFER_SIZE > 0)
				socket.setSendBufferSize(TCP_SEND_BUFFER_SIZE);
			if (TCP_RECEIVE_BUFFER_SIZE > 0)
				socket.setReceiveBufferSize(TCP_RECEIVE_BUFFER_SIZE);
			//Debug.out("NODELAY: " + socket.getTcpNoDelay());
			Debug.out("TCP socket send buffer size: " + socket.getSendBufferSize());
			Debug.out("TCP socket receive buffer size: " + socket.getReceiveBufferSize());
		} catch (SocketException e) {
			Debug.out(e);
		}
	}
}
